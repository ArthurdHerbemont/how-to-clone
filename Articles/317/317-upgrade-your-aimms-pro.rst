Upgrade an AIMMS PRO Installation
=================================
.. meta::
   :description: How to update your AIMMS PRO to a new version.
   :keywords: upgrade, update, PRO, version


The AIMMS PRO upgrade process is designed to be easy! 

Use the installer available from the `AIMMS Downloads page <https://www.aimms.com/english/developers/downloads/download-aimms-pro/>`_.


Prerequisites
---------------

AIMMS PRO requires **Universal Runtime** from **version 2.28** onwards.

Windows 10 comes with this runtime pre-installed. However, some versions or installations of Windows do not include it. 

To manually install the Universal Runtime on the machine that hosts AIMMS PRO, please follow the instructions from `Microsoft <https://support.microsoft.com/en-us/help/3118401/update-for-universal-c-runtime-in-windows>`_ .

