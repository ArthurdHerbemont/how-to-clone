Change Chart View on a Page
======================================

 .. meta::
   :description: How to change the view for 2D charts as an end user.
   :keywords: view, chart, user, scale, size, move

.. note::

    This article was originally posted to the AIMMS Tech Blog.


.. <link>https://berthier.design/aimmsbackuptech/2011/12/28/end-user-feature-for-2d-chart-object/</link>
.. <pubDate>Wed, 28 Dec 2011 09:21:59 +0000</pubDate>
.. <guid isPermaLink="false">http://blog.aimms.com/?p=211</guid>

The end user can change the view of 2D charts (e.g. scaling or moving). This article describes the view options and shortcuts available.

* Scale 
   Hold down both mouse buttons and press ``Ctrl``.
   Move the mouse up to increase the size of the chart.
   Move the mouse down to decrease the size of the chart.

* Move
   Hold down both mouse buttons and press ``Shift``.
   Move the mouse to reposition the chart.

* Zoom 
   Hold down the left mouse button and press ``Ctrl``.
   Drag the mouse to select the area to zoom.

* Rotate 
   Hold down both mouse buttons.
   Move the mouse up or down to rotate.

* Reset
   Press the ``R`` key to reset to the original view. 

