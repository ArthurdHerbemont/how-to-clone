.. image:: ../../../Images/books-128.png
   :scale: 100
   :align: right
   :alt: AIMMS Libraries Help & Support

Libraries
=============================

.. meta::
   :description: Help and support with AIMMS libraries.


Libraries save time and provide valuable resources when developing apps. You can extend regular functionality and share modules among several projects. Find help topics about libraries in AIMMS.

.. toctree::
   :maxdepth: 1

   /Articles/216/216-effective-use-unit-test-library