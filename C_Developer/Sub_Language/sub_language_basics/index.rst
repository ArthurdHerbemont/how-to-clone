.. image:: ../../../Images/code-file-128.png
   :scale: 100
   :align: right
   :alt: AIMMS Language Help & Support

AIMMS Language Basics
=============================

.. meta::
   :description: Help and support with basics of AIMMS language.

These topics cover what you need to get started programming your model in AIMMS language. Get help with identifiers, operators, functions, constraints, and more.

.. toctree::
   :maxdepth: 1

   /Articles/119/119-Reproducible-Random-Sequence 
   /Articles/132/132-Repetive-Patterns-Model-Edit 
   /Articles/194/194-using-loopcount-instead-of-explicit-counting-parameter-in-loops
   /Articles/150/150-solve-in-loop
   /Articles/121/121-set-index-element-parameter
   /Articles/243/243-local-binding-vs-default-binding 
   /Articles/17/17-category-mapping
   /Articles/141/141-element-after-last
   /Articles/112/112-Integer-properties
   /Articles/146/146-value-dynamic-identifier
   /Articles/131/131-Formulas-as-Data
   /Articles/175/175-select-variables-and-constraints-for-math-program
   /Articles/250/250-monitoring-identifiers-for-changes
   /Articles/184/184-use-multiple-indices-for-set
   /Articles/187/187-convert-string-to-number
   /Articles/188/188-use-nbest-operator
   /Articles/190/190-empty-sets
   /Articles/220/220-aborting-execution-of-aimms
   /Articles/12/12-generate-random-numbers
   /Articles/189/189-using-calendars-in-aimms
   /Articles/236/236-get-name-of-current-case
   /Articles/266/266-units-of-measurement-check-equations
   /Articles/266/266-units-of-measurement-localized-data
   /Articles/258/258-save-a-case-from-aimms-procedure
   /Articles/290/290-identifying-differences
   
   