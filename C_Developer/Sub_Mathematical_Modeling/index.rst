.. image:: ../../Images/bricks-128.png
   :scale: 100
   :align: right
   :alt: AIMMS Modeling Help & Support


Mathematical Modeling
========================

.. meta::
   :description: Help and support with creating mathematical models in AIMMS.



Learn how to convert business opportunities into optimization models. This section provides support for designing mathematical models that produce the best solutions to solve problems and maximize opportunities in your business or organization.

.. toctree::
   :maxdepth: 1
    
   /Articles/291/291-solve-to-gmp-with-callback
   /Articles/226/226-color-a-map-with-constraint-programming
   /Articles/138/138-ROGO
   /Articles/137/137-Small-Rostering
   /Articles/142/142-Narrowing-Time-Windows
   /Articles/140/140-Scheduling-Project-Planning
   /Articles/276/276-multi-objective-approach
   /Articles/247/247-minimize-objective-min-operator
   /Articles/249/249-model-variables-with-limited-values
   /Articles/275/275-various-integer-linear-modeling-tricks
 
   
   
