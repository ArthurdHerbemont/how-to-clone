.. image:: ../../../Images/spreadsheet-file-128.png
   :scale: 100
   :align: right
   :alt: AIMMS Excel/CSV Help & Support

Excel and CSV
=============================

.. meta::
   :description: Help and support with connecting AIMMS to Excel and CSV files.


Link your AIMMS model to spreadsheet data in Excel and CSV files.

.. toctree::
   :maxdepth: 1

   /Articles/85/85-using-axll-library   
   /Articles/122/122-AXLL-Library
   /Articles/197/197-reading-data-from-excel
   /Articles/csv/read-write-csv
