.. image:: Images/shooting-star-128.png
   :align: right
   :scale: 100

Recently added
==============

.. keep most recent 10-15 articles

.. Added 11 Oct 2019: 315, 317, 318
.. Added 28 Sep 2019: 310, 312, 313, 314
.. Added 13 Sep 2019: 291,304,305,306,308,309

* :doc:`/Articles/317/317-upgrade-your-aimms-pro`
* :doc:`/Articles/318/318-aimms-and-aimms-pro`
* :doc:`/Articles/315/315-hover-and-select-effects-webui-charts`
* :doc:`/Articles/310/310-investigate-behavior-pro-job`
* :doc:`/Articles/312/312-convert-gams-to-aimms`
* :doc:`/Articles/313/313-get-log-files`
* :doc:`/Articles/314/314-from-dat-to-data`
* :doc:`/Articles/309/309-workflow-panel-element-and-string-parameters`
* :doc:`/Articles/308/308-migrate-pro-server-another-machine`
* :doc:`/Articles/306/306-http-client-library-overview`
* :doc:`/Articles/305/305-number-of-solves`
* :doc:`/Articles/304/304-transfer-license-server`
* :doc:`/Articles/291/291-solve-to-gmp-with-callback`



