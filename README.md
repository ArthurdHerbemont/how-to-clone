Installation
=============


1.   Install Python 2.7.14 from https://www.python.org/downloads/release/python-2714/

1.   run the followings commands in a cmd window

    ```
    python -m pip install --upgrade sphinx
    python -m pip install --upgrade sphinx-rtd-theme
    python -m pip install --upgrade sphinx-sitemap
    ```

1.  clone the repo somewhere on your computer

Build the docs
================

From the repo you've just cloned, run the following in a cmd window

```make html```

The HTML build will be located in the `_build` folder