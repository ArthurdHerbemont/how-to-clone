.. image:: ../../Images/server-128.png
   :scale: 100
   :align: right
   :alt: AIMMS Server Commands Help & Support

Delegate To Server
===================

.. meta::
   :description: Help and support managing server jobs in AIMMS PRO.


.. Need some filler text!

.. toctree::
   :maxdepth: 1

   /Articles/98/98-starting-job-aimms-pro-api-java
   /Articles/98/98-starting-job-aimms-pro-api-csharp
   /Articles/80/80-schedule-jobs