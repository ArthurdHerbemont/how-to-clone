.. image:: ../../Images/paint-tool-128.png
   :scale: 100
   :align: right
   :alt: AIMMS Styling Help & Support


Styling
=======
.. meta::
   :description: Help and support with styling AIMMS applications.


This section contains tips for customizing the appearance of your AIMMS applications using CSS.
    
.. toctree::
   :maxdepth: 1
    
   /Articles/94/94-using-the-scalar-switch-css
   /Articles/49/49-webui-css-color
   /Articles/221/221-adding-a-custom-splash-screen
   /Articles/234/234-align-multiple-objects-winui
   /Articles/315/315-hover-and-select-effects-webui-charts

From the AIMMS Community:

* `3 Tips for a Better Page Layout <https://community.aimms.com/ui-and-ux-40/3-tips-for-a-better-page-layout-223>`_