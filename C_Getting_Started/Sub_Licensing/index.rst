.. image:: ../../Images/key-128.png
   :scale: 100
   :align: right
   :alt: AIMMS Licensing Help & Support

Licensing
========================

.. meta::
   :description: Help and support with licenses.


Here you can find help with license set up and troubleshooting license errors. Includes Network License installation.

.. toctree::
   :maxdepth: 1

   /Articles/195/195-transfer-developer-license
   /Articles/89/89-license-nodelock-error
   /Articles/89/89-academic-license
   /Articles/105/105-adding-named-user-licenses
   /Articles/106/106-install-network-license
   /Articles/304/304-transfer-license-server



